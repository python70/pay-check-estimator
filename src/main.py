#! /usr/bin/env python3
rate = eval(input("What is your hourly wage? "))
hours = eval(input("How many hours did you work these past two weeks? "))
netPercent = eval(input("What percent of your pay do you normaly take home? (in whole numbers) ")) / 100

if hours < 80:
    grosspay = hours * rate
    netPay = hours * rate * netPercent
else:
    pay = hours * rate
    overtimeHours = hours - 80
    overtimePay = overtimeHours * (rate * 0.5)
    netPay = pay + overtimePay
    grossPay = netPay * netPercent

print ("Your bi-weekly gross pay is:", netPay)
print ("Your bi-weekly net pay is:", grossPay)
